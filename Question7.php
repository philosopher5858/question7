<!DOCTYPE html>
<html lang="en">
<head>
<style>
  .header{
    position:relative;
    width: 380px;
    height: 150px;
    border-radius: 10px;
    padding: 40px;
    box-sizing: border-box;
    background:linear-gradient(rgb(73, 73, 255), black);
    box-shadow: 2px 2px 5px #cbced1, -2px -2px 5px white;
    color: #cbced1;
    font-family: 'Courier New', Courier, monospace;
    font-size: 20px;
    
    
  }
  
  body {
    margin: 0;
    width: 100vw;
    height: 100vh;
    background: radial-gradient(rgb(67, 67, 255), black);
    display: flex;
    align-items: center;
    text-align: center;
    justify-content: center;
    place-items: center;
    overflow: hidden;
  }
  
  .container {
    color: aliceblue;
    position: relative;
    width: 300px;
    height: 200px;
    border-radius: 20px;
    padding: 40px;
    /* box-sizing: border-box; */
    background: radial-gradient(blue,black);
    box-shadow: 2px 2px 5px #cbced1, -2px -2px 5px white;
    
  }
  
  .inputs {
    text-align: left;
    margin-top: 3px;
  }
  
  label, input, button {
    display:inline;
    width: 100%;
    padding: 0;
    border: none;
    outline: none;
    box-sizing: border-box;
  }
  .lable2{
    margin-bottom: 40px;
    padding-right: 100%;
    font-family: 'Courier New', Courier, monospace;
  }
  
  label {
    margin-bottom: 40px;
    padding-right: 100%;
    font-family: 'Courier New', Courier, monospace;
  }
  
  label:nth-of-type(2) {
    margin-top: 12px;
    padding-right: 0%;
  }
  
  input::placeholder {
    color: darkgray;
    font-family:monospace
  }
  
  input {
    background: #f7f9fb;
    padding: 10px;
    padding-left: 20px;
    height: 50px;
    font-size: 14px;
    border-radius: 20px;
    box-shadow: inset 6px 6px 6px #7da9d6, inset -6px -6px 6px rgb(205, 246, 249);
  }
  
  button {
    color: white;
    margin-top: 20px;
    background: linear-gradient(black,blue);
    height: 40px;
    border-radius: 20px;
    cursor: pointer;
    font-weight: 900;
    transition: 0.5s;
  }
  
  button:hover {
    box-shadow: none;
    box-shadow: 2px 2px 2px #cbced1, -2px -2px 2px white;
  }
  
  a {
    position: absolute;
    font-size: 8px;
    bottom: 4px;
    right: 4px;
    text-decoration: none;
    color: black;
    /* background: yellow; */
    border-radius: 10px;
    padding: 2px;
  }
    </style>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="Question7.html">
<div class="header">WAP to calculate 
        (a+b)<sup>2</sup>
        (a+b)<sup>3</sup>
        (a<sup>2</sup>-b<sup>2</sup>)
    </div>
    <br><br>
    <DIv class="container">
    
        
        <?php
if($_SERVER['REQUEST_METHOD']=='POST'){

    if(empty($_POST['a'])){
        $A = "Value Require";
    }else{
        $A = $_POST['a'];
    }
    if(empty($_POST['b'])){
        $B = "Value Require";
    }else{
        $B = ($_POST['b']);
    }
    print "INPUT"."<br>";
    echo "a = ".$A."<br>";
    echo "b = ".$B."<br><br>";

   print "OUTPUT"."<br>";
    echo "(a+b)"."<sup>2"."</sup> = ".(($A + $B)**2)."<br>";
    echo "(a+b)"."<sup>3"."</sup> = ".(($A + $B)**3)."<br>";
    echo "("."a"."<sup>2"."</sup>"."- b"."<sup>2"."</sup>".") = ".((($A)**2) - (($B)**2));

}
?>

<button type="submit">GO BACK</button>

    
                    
        </DIv>
</form>
    
</body>
</html>


